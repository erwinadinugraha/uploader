/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worker;

import Model.Csv;
import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author ASUS
 */
public class ThreadReaderWorker implements Runnable {

    static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    static final String DB_URL = "jdbc:oracle:thin:@10.254.62.38:1521:pajak";
    static final String USER = "newefiling";
    static final String PASS = "newefiling";

    private DefaultTableModel model;
    private int Counter_array_row;

    private Csv csv;

    public ThreadReaderWorker(Csv csv, DefaultTableModel model) {
        this.csv = csv;
        this.model = model;
    }

    @Override
    public void run() {
        try {
            String filePathString = "E:\\backupcsv\\" + csv.getNama_csv();
            File f = new File(filePathString);
            if (!f.exists()) {
                System.out.println("Error: File Not Exist");
                return;
            } else {
                System.out.println("File Exist");
            }
            int length = Thread.currentThread().getName().length();
            Thread.currentThread().setName(Thread.currentThread().getName().substring(length - 1));

            Counter_array_row = Integer.valueOf(Thread.currentThread().getName());
//            Counter_array_row--;
            System.out.println("Counter array row nya " + Counter_array_row);
            int min = 1;
            int max = 10;

            int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
            model.setValueAt(csv.getNama_csv(), Counter_array_row, 1);
            model.setValueAt(csv.getJml_detil() + " line", Counter_array_row, 2);
            model.setValueAt(randomNum, Counter_array_row, 3);
            model.setValueAt("PROSES", Counter_array_row, 4);
            TimeUnit.SECONDS.sleep(randomNum);
            model.setValueAt("SUKSES", Counter_array_row, 4);
            TimeUnit.SECONDS.sleep(1);

            model.setValueAt("", Counter_array_row, 1);
            model.setValueAt("", Counter_array_row, 2);
            model.setValueAt("", Counter_array_row, 3);
            model.setValueAt("", Counter_array_row, 4);
            TimeUnit.SECONDS.sleep(1);
        } catch (Exception ex) {
            Logger.getLogger(ThreadReaderWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
