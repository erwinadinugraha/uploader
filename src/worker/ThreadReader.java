/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worker;

import java.math.BigDecimal;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ASUS
 */
public class ThreadReader extends Thread {

    static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    static final String DB_URL = "jdbc:oracle:thin:@10.254.62.38:1521:pajak";
    static final String USER = "newefiling";
    static final String PASS = "newefiling";

    private Thread t;
    private DefaultTableModel model;
    private int No_thread;

    private int Counter_array_row;

    public ThreadReader(int counter_array_row, int no_thread, DefaultTableModel model1) {
        model = model1;
        Counter_array_row = counter_array_row;
        No_thread = no_thread;
//        System.out.println("Creating Thread = " + No_thread);
    }

    public void run() {

        try {
            // set timer for scheduling running
            Timer timer = new Timer();
            timer.schedule(new Proses(), 0, 1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
//        System.out.println("Starting Thread = " + No_thread);
        if (t == null) {
            t = new Thread(this, String.valueOf("Thread"+ No_thread));
            t.start();
        }
    }

    private class Proses extends TimerTask {

        private PreparedStatement preparedStatement;

        public Proses() {
        }

        @Override
        public void run() {
            try {

                Statement st;
                Connection conn;
                ResultSet rs = null;
                Class.forName(JDBC_DRIVER);
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
                st = conn.createStatement();
                String sql = "SELECT\n"
                        + "	ID_INTERFACE_HD,\n"
                        + "	JML_DETIL,\n"
                        + "	NAMA_CSV\n"
                        + "FROM\n"
                        + "	PP_ESPT_HEADER2\n"
                        + "WHERE\n"
                        + "	ID_STATUS = '11'\n"
                        + "ORDER BY\n"
                        + "	CREATION_DATE ASC";
                rs = st.executeQuery(sql);

                while (rs.next()) {
                    try {
//                        conn.setAutoCommit(false);
                        model.setValueAt(rs.getString("NAMA_CSV"), Counter_array_row, 1);
                        model.setValueAt(rs.getString("JML_DETIL"), Counter_array_row, 2);
                        model.setValueAt("-----------------", Counter_array_row, 3);
                        model.setValueAt("BELUM BERHASIL", Counter_array_row, 4);
                        TimeUnit.SECONDS.sleep(2);
                        model.setValueAt("SUKSES", Counter_array_row, 4);
//                        preparedStatement = conn.prepareStatement("UPDATE ASSIGN_UPLOAD set STATUS_UPLOADED = ? WHERE ID_INTERFACE_HD = ?");
//                        preparedStatement.setInt(1, 1);
//                        preparedStatement.setBigDecimal(2, rs.getBigDecimal("ID_INTERFACE_HD"));

//                        preparedStatement.executeUpdate();
                        TimeUnit.SECONDS.sleep(2);
//                        conn.commit();
                    } catch (SQLException ex) {
                        conn.rollback();
                    }
                }
                model.setValueAt("", Counter_array_row, 1);
                model.setValueAt("", Counter_array_row, 2);
                model.setValueAt("", Counter_array_row, 3);
                model.setValueAt("", Counter_array_row, 4);

                rs.close();
                st.close();
                conn.close();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ThreadReader.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ThreadReader.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(ThreadReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
