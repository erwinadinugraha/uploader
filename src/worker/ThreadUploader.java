/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worker;

import java.math.BigDecimal;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ASUS
 */
public class ThreadUploader extends Thread {

    static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    static final String DB_URL = "jdbc:oracle:thin:@10.254.62.38:1521:pajak";
    static final String USER = "newefiling";
    static final String PASS = "newefiling";

    private Thread t;
    private String threadName;
    private DefaultTableModel model;
    private int Size;
    private int No_thread;
    private int Counter_array_row;

    public ThreadUploader(int counter_array_row, int size, int no_thread, DefaultTableModel model1) {
        threadName = "THREAD_" + generate_category(size) + "_" + no_thread + "";
        model = model1;
        Counter_array_row = counter_array_row;
        Size = size;
        No_thread = no_thread;
    }

    public void run() {

        try {
            // set timer for scheduling running
            Timer timer = new Timer();
            timer.schedule(new Proses(), 0, 2000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        System.out.println("Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
//            monitorThread(t);
        }
    }
    
    private static void monitorThread(Thread monitorMe) {
         while (monitorMe.isAlive()) {
            try {
                StackTraceElement[] threadStacktrace = monitorMe.getStackTrace();

                System.out.println(monitorMe.getName() + " is Alive and it's state =" + monitorMe.getState() + " ||  Execution is in method : (" + threadStacktrace[0].getClassName() + "::" + threadStacktrace[0].getMethodName() + ") @line" + threadStacktrace[0].getLineNumber());

                TimeUnit.MILLISECONDS.sleep(700);
            } catch (Exception ex) {
            }
            /* since threadStacktrace may be empty upon reference since Thread A may be terminated after the monitorMe.getStackTrace(); call*/
        }
        System.out.println(monitorMe.getName() + " is dead and its state =" + monitorMe.getState());
    
    }

    private class Proses extends TimerTask {

        private PreparedStatement preparedStatement;

        public Proses() {
        }

        @Override
        public void run() {
            try {
                Statement stmt_THR;
                Connection conn_small1;
                // print timestamp to console
                String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
//                System.out.println("Running di : " +timeStamp);
                ResultSet rs_THR = null;
                Class.forName(JDBC_DRIVER);
                conn_small1 = DriverManager.getConnection(DB_URL, USER, PASS);
                stmt_THR = conn_small1.createStatement();
                String sqlsmall1 = "SELECT\n"
                        + "	ASSIGN_UPLOAD.*, PP_ESPT_HEADER2.JML_DETIL\n"
                        + "FROM\n"
                        + "	ASSIGN_UPLOAD\n"
                        + "LEFT JOIN PP_ESPT_HEADER2 ON ASSIGN_UPLOAD.ID_INTERFACE_HD = PP_ESPT_HEADER2.ID_INTERFACE_HD\n"
                        + "WHERE\n"
                        + "	ASSIGN_UPLOAD.CATEGORY_SIZE = " + Size + "\n"
                        + "AND ASSIGN_UPLOAD.NO_THREAD = " + No_thread + "\n"
                        + "AND (ASSIGN_UPLOAD.STATUS_UPLOADED = 0 OR ASSIGN_UPLOAD.STATUS_UPLOADED is NULL)";
                rs_THR = stmt_THR.executeQuery(sqlsmall1);

                while (rs_THR.next()) {
                    try {
//                        conn_small1.setAutoCommit(false);
                        System.out.println(threadName + " Got Data : ID_INTERFACE_HD= " + rs_THR.getString("ID_INTERFACE_HD"));
                        model.setValueAt(rs_THR.getString("ID_INTERFACE_HD"), Counter_array_row, 2);
                        model.setValueAt(rs_THR.getString("JML_DETIL"), Counter_array_row, 3);
                        model.setValueAt("-----------------", Counter_array_row, 4);
                        model.setValueAt("BELUM BERHASIL", Counter_array_row, 5);
                        TimeUnit.SECONDS.sleep(2);
                        model.setValueAt("SUKSES", Counter_array_row, 5);
//                        preparedStatement = conn_small1.prepareStatement("UPDATE ASSIGN_UPLOAD set STATUS_UPLOADED = ? WHERE ID_INTERFACE_HD = ?");
//                        preparedStatement.setInt(1, 1);
//                        preparedStatement.setBigDecimal(2, rs_THR.getBigDecimal("ID_INTERFACE_HD"));
//
//                        preparedStatement.executeUpdate();
                        TimeUnit.SECONDS.sleep(1);
//                        conn_small1.commit();
                    } catch (SQLException ex) {
                        conn_small1.rollback();
                    }
                }
                model.setValueAt("", Counter_array_row, 2);
                model.setValueAt("", Counter_array_row, 3);
                model.setValueAt("", Counter_array_row, 4);
                model.setValueAt("", Counter_array_row, 5);

                rs_THR.close();
                stmt_THR.close();
                conn_small1.close();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ThreadUploader.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ThreadUploader.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(ThreadUploader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private String generate_category(int aInt) {
        if (aInt == 1) {
            return "SMALL";  // SMALL 
        } else if (aInt == 2) {
            return "MEDIUM";  // MEDIUM 
        } else {
            return "LARGE";  // LARGE
        }
    }
}
