/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author ASUS
 */
public class Csv {
    
    private String nama_csv;
    private String jml_detil;
    private int status;

    public Csv(String nama_csv, String jml_detil, int status) {
        this.nama_csv = nama_csv;
        this.jml_detil = jml_detil;
        this.status = status;
    }

    public String getNama_csv() {
        return nama_csv;
    }

    public void setNama_csv(String nama_csv) {
        this.nama_csv = nama_csv;
    }

    public String getJml_detil() {
        return jml_detil;
    }

    public void setJml_detil(String jml_detil) {
        this.jml_detil = jml_detil;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


}
